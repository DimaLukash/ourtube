const express = require('express');
const cors = require('cors');
const path = require('path');
const videoControllers = require('./controllers/video');
const { serverPort } = require('./config');

const app = express();

app.use(cors());
app.use(express.static(path.join(__dirname, 'static')));
app.use('/video', videoControllers);

app.listen(serverPort, () => {
  console.log(`Server started at http://localhost:${serverPort}`);
});
