const ffmpeg = require('fluent-ffmpeg');

const { thumbnailPath } = require('../../config');

const postVideo = async (req, res) => {
  for (const file of req.files) {
    await ffmpeg(file.path).screenshots({
      timestamps: ['10%'],
      filename: `%b-thumbnail.jpg`,
      folder: thumbnailPath,
      size: '800x?',
    });
  }

  res.sendStatus(200);
};

module.exports = postVideo;
