const fs = require('fs');
const path = require('path');
const btoa = require('btoa');

const config = require('../../config');

const getVideoList = async (req, res) => {
  const { mediaFolder, thumbnailFolder, mediaPath, serverPort } = config;
  const { searchQuery } = req.query;

  try {
    await fs.promises.stat(mediaPath);
  } catch (e) {
    res.send([]);
    return;
  }

  const files = await fs.promises.readdir(mediaPath, { withFileTypes: true });

  const result = files
    .filter(
      file =>
        file.isFile() &&
        (!searchQuery ||
          file.name.toLowerCase().includes(searchQuery.toLowerCase())),
    )
    .map(({ name }) => ({
      videoName: name,
      videoId: btoa(path.basename(name)),
      thumbnailUrl: `http://localhost:${serverPort}/${mediaFolder}/${thumbnailFolder}/${path.basename(
        name,
        path.extname(name),
      )}-thumbnail.jpg`,
    }));

  res.send(result);
};

module.exports = getVideoList;
