const atob = require('atob');
const fs = require('fs');
const path = require('path');
const { mediaPath, mediaFolder, serverPort } = require('../../config');

const getVideo = async (req, res) => {
  const { id } = req.params;
  let videoName;
  try {
    videoName = atob(id);
    await fs.promises.stat(path.join(mediaPath, videoName));
  } catch (e) {
    res.sendStatus(404);
    return;
  }

  res.send({
    videoUrl: `http://localhost:${serverPort}/${mediaFolder}/${videoName}`,
    videoName,
  });
};

module.exports = getVideo;
