const express = require('express');
const router = express.Router();
const multer = require('multer');

const getVideoList = require('./getVideoList');
const getVideo = require('./getVideo');
const postVideo = require('./postVideo');

const { mediaPath } = require('../../config');

const storage = multer.diskStorage({
  destination: mediaPath,
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage });

router.get('/', getVideoList);
router.get('/:id', getVideo);
router.post('/', upload.array('videos'), postVideo);

module.exports = router;
