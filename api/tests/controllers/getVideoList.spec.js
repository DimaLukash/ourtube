const { describe, it, afterEach, before } = require('mocha');
const Sinon = require('sinon');
require('chai').should();
const path = require('path');
const config = require('../../config');

const getVideoList = require('../../controllers/video/getVideoList');

describe('video controller', () => {
  const sandbox = Sinon.createSandbox();
  let req;
  let res;

  before(() => {
    sandbox.stub(config, 'mediaFolder').value('media');
    sandbox.stub(config, 'thumbnailFolder').value('thumbnail');
    sandbox
      .stub(config, 'mediaPath')
      .value(path.join(__dirname, '../testFiles'));
    sandbox.stub(config, 'serverPort').value('3000');
  });

  beforeEach(() => {
    req = {query: {searchQuery: ''}}
    res = {
      send: sandbox.stub(),
    };
  })

  afterEach(() => {
    sandbox.reset();
  });

  after(() => {
    sandbox.restore();
  })

  describe('getVideoList', () => {
    it('should return list with video data', async () => {
      await getVideoList(req, res);
      res.send.getCall(0).args.should.deep.equal([
        [
          {
            thumbnailUrl:
              'http://localhost:3000/media/thumbnail/test-thumbnail.jpg',
            videoId: 'dGVzdC5hdmk=',
            videoName: 'test.avi'
          },
        ],
      ]);
    });

    it('should return list with video data which matched searchQuery', async () => {
      req.query.searchQuery = 'test'
      await getVideoList(req, res);
      res.send.getCall(0).args.should.deep.equal([
        [
          {
            thumbnailUrl:
                'http://localhost:3000/media/thumbnail/test-thumbnail.jpg',
            videoId: 'dGVzdC5hdmk=',
            videoName: 'test.avi'
          },
        ],
      ]);
    });

    it('should return an empty list when all videos not matched searchQuery', async () => {
      req.query.searchQuery = 'search'
      await getVideoList(req, res);
      res.send.getCall(0).args.should.deep.equal([[]]);
    });
  });
});
