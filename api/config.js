const path = require('path');
require('dotenv').config();

const mediaPath = path.join(__dirname, 'static', process.env.MEDIA_FOLDER);

module.exports = {
  serverPort: process.env.REACT_APP_SERVER_PORT,
  mediaPath,
  mediaFolder: process.env.MEDIA_FOLDER,
  thumbnailPath: path.join(mediaPath, process.env.THUMBNAIL_FOLDER),
  thumbnailFolder: process.env.THUMBNAIL_FOLDER,
};
