import { render, screen } from '@testing-library/react';

import Watch from '../../pages/Watch';

let mockIsLoading;
let mockIsVideoExist;
let mockVideo;

jest.mock('../../pages/Watch/hooks/useFetchVideo', () => {
  return jest.fn(() => ({
    isLoading: mockIsLoading,
    isVideoExist: mockIsVideoExist,
    video: mockVideo,
  }));
});

describe('Watch', () => {
  beforeEach(() => {
    mockIsLoading = false;
    mockIsVideoExist = false;
    mockVideo = { videoUrl: 'url' };
  });

  test('should render video element', () => {
    mockIsVideoExist = true;
    render(<Watch />);

    const videoElement = screen.getByTestId('video');
    expect(videoElement).toBeInTheDocument();
  });

  test("should show warning message when video doesn't exist", () => {
    render(<Watch />);

    const alertElement = screen.getByRole('alert');
    expect(alertElement).toBeInTheDocument();
  });

  test('should show spinner when data is loading', () => {
    mockIsLoading = true;
    render(<Watch />);
    const spinnerElement = screen.getByTestId('spinner');
    expect(spinnerElement).toBeInTheDocument();
  });
});
