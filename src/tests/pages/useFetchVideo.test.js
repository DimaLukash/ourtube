import { renderHook } from '@testing-library/react-hooks';
import { toast } from 'react-toastify';
import useFetchVideo from '../../pages/Watch/hooks/useFetchVideo';
import { get } from '../../common/axiosClient';

jest.mock('react-router-dom', () => ({
  useParams: () => {
    return { id: 1 };
  },
}));

jest.mock('../../common/axiosClient', () => ({
  get: jest.fn(),
}));

jest.mock('react-toastify', () => ({
  toast: { error: jest.fn() },
}));

describe.only('useFetchVideo', () => {
  beforeEach(() => {
    jest.resetAllMocks();
    jest.resetModules();
    get.mockImplementation(() => ({ data: 'url' }));
  });

  test('should return correct result when video exist', async () => {
    const { result, waitForNextUpdate } = renderHook(() => useFetchVideo());
    await waitForNextUpdate();
    expect(result.current).toEqual({
      isLoading: false,
      isVideoExist: true,
      video: 'url',
    });
  });

  test("should return correct result when video doesn't exist", async () => {
    get.mockImplementation(() => {
      throw { response: { status: 404 } };
    });

    const { result, waitForNextUpdate } = renderHook(() => useFetchVideo());
    // await waitForNextUpdate();
    expect(result.current).toEqual({
      isLoading: false,
      isVideoExist: false,
      video: {},
    });
  });

  test('should return correct result and show notification when something went wrong', async () => {
    get.mockImplementation(() => {
      throw new Error();
    });

    const { result } = renderHook(() => useFetchVideo());

    expect(result.current).toEqual({
      isLoading: false,
      isVideoExist: false,
      video: {},
    });

    expect(toast.error).toBeCalled();
  });
});
