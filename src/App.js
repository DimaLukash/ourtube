import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { ToastContainer } from 'react-toastify';

import Home from './pages/Home';
import Watch from './pages/Watch';
import Uploads from './pages/Uploads';
import NavigationBar from './pages/NavigationBar';

import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <>
      <Router>
        <NavigationBar />
        <Switch>
          <Route path={'/watch/:id'} component={Watch} />
          <Route path="/uploads" component={Uploads} />
          <Route path="/home" component={Home} />
          <Redirect to={'/home'} />
        </Switch>
      </Router>
      <ToastContainer />
    </>
  );
}

export default App;
