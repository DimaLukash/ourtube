import React, { useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { Row, Col, Container, Alert } from 'react-bootstrap';
import { isEmpty } from 'lodash';

import Helmet from '../../common/Helmet';
import Spinner from '../../common/Spinner';
import VideoPreview from './VideoPreview/VideoPreview';
import useFetchVideoList from './hooks/useFetchVideoList';

const Home = () => {
  const { isLoading, videoList } = useFetchVideoList();

  const history = useHistory();
  const handlePreviewClick = useCallback(
    videoId => {
      history.push(`/watch/${videoId}`);
    },
    [history],
  );

  const isVideoListEmpty = isEmpty(videoList) && !isLoading;

  return (
    <Container>
      <Helmet title={'Home page'} />
      <Row>
        <h1>Home</h1>
      </Row>
      <Container>
        {isVideoListEmpty && (
          <Alert variant="warning">There is no videos</Alert>
        )}
        {!isLoading ? (
          videoList.map(({ thumbnailUrl, videoId, videoName }) => {
            return (
              <Row key={videoId}>
                <Col lg={2} />
                <Col lg={8}>
                  <VideoPreview
                    onClick={handlePreviewClick}
                    thumbnailUrl={thumbnailUrl}
                    videoId={videoId}
                    videoName={videoName}
                  />
                </Col>
                <Col lg={2} />
              </Row>
            );
          })
        ) : (
          <Spinner />
        )}
      </Container>
    </Container>
  );
};

export default Home;
