import React, { useCallback } from 'react';
import { Card } from 'react-bootstrap';
import classNames from 'classnames';
import PropTypes from 'prop-types';

import style from './videoPreview.module.css';

const VideoPreview = ({ thumbnailUrl, videoId, onClick, videoName }) => {
  const handleClick = useCallback(
    e => {
      e.preventDefault();
      onClick(videoId);
    },
    [videoId, onClick],
  );

  return (
    <Card
      className={classNames(style.preview, 'text-center', 'mb-2')}
      onClick={handleClick}
    >
      <Card.Img width={300} src={thumbnailUrl} />
      <Card.Body>
        <Card.Title>{videoName}</Card.Title>
      </Card.Body>
    </Card>
  );
};

VideoPreview.propTypes = {
  thumbnailUrl: PropTypes.string.isRequired,
  videoId: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  videoName: PropTypes.string.isRequired,
};

export default VideoPreview;
