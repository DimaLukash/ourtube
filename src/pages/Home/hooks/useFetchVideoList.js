import { useCallback, useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import axiosClient from '../../../common/axiosClient';
import { toast } from 'react-toastify';

const useFetchVideoList = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [videoList, setVideoList] = useState([]);
  const location = useLocation();
  const params = new URLSearchParams(location.search);
  const searchQuery = params.get('searchQuery');

  const fetchVideoList = useCallback(async () => {
    setIsLoading(true);
    try {
      const { data } = await axiosClient.get('/video', {
        params: { searchQuery },
      });
      setVideoList(data);
    } catch (e) {
      console.error(e);
      toast.error('Something went wrong. Please try again later');
    } finally {
      setIsLoading(false);
    }
  }, [searchQuery]);

  useEffect(() => {
    fetchVideoList();
  }, [fetchVideoList]);

  return {
    isLoading,
    videoList,
  };
};

export default useFetchVideoList;
