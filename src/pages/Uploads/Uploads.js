import React from 'react';
import { ProgressBar, Container, Row, Col } from 'react-bootstrap';

import Helmet from '../../common/Helmet';
import useHandleUpload from './hooks/useHandleUpload';
import useDragAndDrop from './hooks/useDragAndDrop';

const Uploads = () => {
  const { handleUpload, isLoading, uploadProgress } = useHandleUpload();

  const { isDragActive, rootProps, inputProps } = useDragAndDrop({
    onUpload: handleUpload,
  });

  return (
    <Container>
      <Helmet title={'Upload'} />
      <Row>
        <h1>Upload</h1>
      </Row>
      <Row>
        <Col className="text-center">
          {!isLoading ? (
            <div {...rootProps}>
              <input {...inputProps} />
              {isDragActive ? (
                <p>Drop the files here ...</p>
              ) : (
                <p>Drag 'n' drop some videos here, or click to select videos</p>
              )}
            </div>
          ) : (
            <ProgressBar now={uploadProgress} />
          )}
        </Col>
      </Row>
    </Container>
  );
};

export default Uploads;
