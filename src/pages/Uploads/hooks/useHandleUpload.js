import { useCallback, useState } from 'react';
import axiosClient from '../../../common/axiosClient';
import { toast } from 'react-toastify';

const useHandleUpload = () => {
  const [uploadProgress, setUploadProgress] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  const handleUpload = useCallback(
    async files => {
      setIsLoading(true);
      const formData = new FormData();
      files.forEach(file => {
        formData.append('videos', file);
      });

      try {
        await axiosClient.post('/video', formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
          },
          onUploadProgress: progressEvent => {
            setUploadProgress(
              Math.round((progressEvent.loaded * 100) / progressEvent.total),
            );
          },
        });

        toast.success('Success');
      } catch (e) {
        console.error(e);
        toast.error('Something went wrong. Please try again later');
      } finally {
        setUploadProgress(0);
        setIsLoading(false);
      }
    },
    [setUploadProgress, setIsLoading],
  );
  return {
    handleUpload,
    uploadProgress,
    isLoading,
  };
};

export default useHandleUpload;
