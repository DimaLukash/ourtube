import React from 'react';
import { Container, Row, Col, Alert } from 'react-bootstrap';
import Helmet from '../../common/Helmet';
import Spinner from '../../common/Spinner';

import useFetchVideo from './hooks/useFetchVideo';

import style from './watch.module.css';

const Watch = () => {
  const { isVideoExist, video, isLoading } = useFetchVideo();

  const content = isVideoExist ? (
    <video
      data-testid={`video`}
      className={style.video}
      src={video.videoUrl}
      controls
    />
  ) : (
    <Alert variant="warning">Video doesn't exist</Alert>
  );

  return (
    <Container>
      <Helmet title={'Watch'} />
      <Row>
        <h1>Watch</h1>
      </Row>

      <Row>
        <Col className="text-center">{!isLoading ? content : <Spinner />}</Col>
      </Row>
    </Container>
  );
};

export default Watch;
