import { useParams } from 'react-router-dom';
import { useCallback, useEffect, useState } from 'react';
import axiosClient from '../../../common/axiosClient';
import { toast } from 'react-toastify';

const useFetchVideo = () => {
  const { id } = useParams();
  const [isLoading, setIsLoading] = useState(true);
  const [video, setVideo] = useState({});
  const [isVideoExist, setIsVideoExist] = useState(true);

  const fetchVideo = useCallback(async () => {
    setIsLoading(true);

    try {
      const { data } = await axiosClient.get(`/video/${id}`);
      setVideo(data);
    } catch (e) {
      setIsVideoExist(false);

      if (e?.response?.status !== 404) {
        toast.error('Something went wrong. Please try again later');
      }
    } finally {
      setIsLoading(false);
    }
  }, [id]);

  useEffect(() => {
    fetchVideo();
  }, [fetchVideo]);

  return {
    isLoading,
    isVideoExist,
    video,
  };
};

export default useFetchVideo;
