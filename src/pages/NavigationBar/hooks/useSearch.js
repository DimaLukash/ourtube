import { useHistory } from 'react-router-dom';
import { useCallback, useState } from 'react';

const useSearch = () => {
  const [searchQuery, setSearchQuery] = useState('');
  const history = useHistory();

  const handleSearchChange = useCallback(e => {
    setSearchQuery(e.target.value);
  }, []);

  const handleSubmit = useCallback(
    e => {
      e.preventDefault();
      e.stopPropagation();

      history.push(`/home?searchQuery=${searchQuery}`);
    },
    [history, searchQuery],
  );

  return {
    handleSearchChange,
    handleSubmit,
    searchQuery,
  };
};

export default useSearch;
