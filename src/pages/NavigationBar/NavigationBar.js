import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import {
  Navbar,
  Container,
  Button,
  FormControl,
  Nav,
  Form,
} from 'react-bootstrap';
import useSearch from './hooks/useSearch';

const NavigationBar = () => {
  const { pathname } = useLocation();
  const { handleSearchChange, handleSubmit, searchQuery } = useSearch();

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        <Navbar.Brand as={Link} to="/home">
          OurTube
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link active={pathname === '/home'} as={Link} to="/home">
              Home
            </Nav.Link>
            <Nav.Link active={pathname === '/uploads'} as={Link} to="/uploads">
              Upload
            </Nav.Link>
          </Nav>
          <Form inline onSubmit={handleSubmit}>
            <FormControl
              value={searchQuery}
              onChange={handleSearchChange}
              type="text"
              placeholder="Search"
              className="mr-sm-2"
            />
            <Button type="submit">Search</Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default NavigationBar;
