import PropTypes from 'prop-types';
import { Helmet as ReactHelmet } from 'react-helmet';

const Helmet = ({ title }) => {
  return (
    <ReactHelmet>
      <meta charSet="utf-8" />
      <title>{title}</title>
    </ReactHelmet>
  );
};

Helmet.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Helmet;
