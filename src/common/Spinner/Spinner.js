import React from 'react';
import { Spinner as BootstrapSpinner } from 'react-bootstrap';

const Spinner = () => {
  return (
    <div data-testid={`spinner`} className="d-flex justify-content-center">
      <BootstrapSpinner animation="border" />
    </div>
  );
};

export default Spinner;
